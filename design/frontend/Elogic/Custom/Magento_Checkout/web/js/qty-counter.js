define( [
    "jquery",
    "Magento_Checkout/js/action/get-totals",
    "Magento_Customer/js/customer-data",
    'domReady!'
],function($, getTotalsAction, customerData){
    return function (config, element) {
        $('button').on('click',
            function(){

                var $this = $(this);
                var ctrl = ($(this).attr('id').replace('-upt', '')).replace('-dec', '');
                var currentQty = $("#cart-" + ctrl + "-qty").val();
                if ($this.hasClass('increaseQty-' + config.buttonId)) {
                    var newAdd = parseInt(currentQty) + parseInt(1);
                    $("#cart-" + ctrl + "-qty").val(newAdd);
                } else if ($this.hasClass('decreaseQty-' + config.buttonId)) {
                    if (currentQty > 0) {
                        var newAdd = parseInt(currentQty) - parseInt(1);
                        $("#cart-" + ctrl + "-qty").val(newAdd);
                    }
                }
                var form = $('form#form-validate');
                $.ajax({
                    url: config.ajaxUrl,
                    type: "POST",
                    data: form.serialize(),
                    showLoader: true,
                    cache: false,
                    success: function(res){
                        var parsedResponse = $.parseHTML(res);
                        var result = $(parsedResponse).find("#form-validate");
                        var sections = ['cart'];
                        $("#form-validate").replaceWith(result);
                        // The mini cart reloading
                        customerData.reload(sections, true);

                        // The totals summary block reloading
                        var deferred = $.Deferred();
                        getTotalsAction([], deferred);
                    },
                    error: function (xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message);
                    }
                });
                return true;
            });
    };
});
